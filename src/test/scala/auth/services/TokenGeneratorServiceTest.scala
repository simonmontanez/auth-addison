package auth.services

import java.time.Clock

import auth.MockClock
import auth.domain.{User, UserToken}
import org.scalatest.{Matchers, WordSpec}

class TokenGeneratorServiceTest extends WordSpec with Matchers {

  object TokenGenerator extends TokenGeneratorService
  implicit val clock: MockClock = new MockClock(Clock.systemUTC())

  "TokenGeneratorService" should {
    "generate token with the expected format" in {
      TokenGenerator.generateToken(User("smontanez")) shouldBe Right(
        UserToken(s"smontanez_${clock.instant().toString}"))
    }

    "validate User" in {
      TokenGenerator.isValidUser(User("smontanez")) shouldBe true
      TokenGenerator.isValidUser(User("Asmontanez")) shouldBe false
    }
  }
}
