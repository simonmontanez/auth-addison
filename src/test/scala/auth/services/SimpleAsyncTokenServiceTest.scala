package auth.services

import akka.actor.{ActorRef, ActorSystem, Props}
import akka.testkit.TestKit
import akka.util.Timeout
import auth.domain.{Credentials, UserToken}
import auth.errors.AuthError
import auth.http.DefaultConfig
import org.scalatest.{Matchers, AsyncWordSpecLike}

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._

class SimpleAsyncTokenServiceTest
    extends TestKit(ActorSystem("SimpleAsyncTokenServiceTest"))
    with AsyncWordSpecLike
    with Matchers {

  object SimpleAsyncToken extends SimpleAsyncTokenService {
    override val tokenGeneratorActor: ActorRef =
      system.actorOf(Props[TokenGeneratorActor])
    override val validateCredentialActor: ActorRef =
      system.actorOf(Props[ValidateCredentialActor])
    override implicit val askTimeout: Timeout =
      DefaultConfig.akkaActorTimeout.seconds
    override implicit val executionContext: ExecutionContext = system.dispatcher
  }

  "SimpleAsyncTokenService" should {
    "generate token" in {
      SimpleAsyncToken.requestToken(Credentials("smontanez", "SMONTANEZ")) map {
        x =>
          x shouldBe a[UserToken]
      }
    }

    "fail generating a token given bad password" in {
      SimpleAsyncToken
        .requestToken(Credentials("smontanez", "xxxx"))
        .failed map (x => x shouldBe a[AuthError])
    }

    "fail generating a token given bad user" in {
      SimpleAsyncToken
        .requestToken(Credentials("Amontanez", "AMONTANEZ"))
        .failed map (x => x shouldBe a[AuthError])
    }
  }
}
