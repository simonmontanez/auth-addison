package auth.services

import akka.actor.{ActorSystem, Props}
import akka.testkit.{ImplicitSender, TestKit}
import auth.domain.{Credentials, User}
import auth.errors.InvalidCredentials
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}
import scala.concurrent.duration._

class ValidateCredentialActorTest()
    extends TestKit(ActorSystem("ValidateCredentialActorTest"))
    with ImplicitSender
    with WordSpecLike
    with Matchers
    with BeforeAndAfterAll {

  override def afterAll {
    TestKit.shutdownActorSystem(system)
  }

  "Validate Credential Actor" must {
    "return an User if the given credentials are valid" in {

      val validateCredentialActor =
        system.actorOf(Props[ValidateCredentialActor])
      validateCredentialActor ! Credentials("smontanez", "SMONTANEZ")

      expectMsg(5.seconds, User("smontanez"))

    }

    "return error given an invalid credentials" in {

      val validateCredentialActor =
        system.actorOf(Props[ValidateCredentialActor])
      validateCredentialActor ! Credentials("smontanez", "SmonTANEZ")

      expectMsg(5.seconds, InvalidCredentials)

    }

  }
}
