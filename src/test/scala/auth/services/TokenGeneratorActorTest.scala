package auth.services

import akka.actor.{ActorSystem, Props}
import akka.testkit.{ImplicitSender, TestKit}
import auth.domain.{User, UserToken}
import auth.errors.AuthError
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}

import scala.concurrent.duration._

class TokenGeneratorActorTest
    extends TestKit(ActorSystem("TokenGeneratorActorTest"))
    with ImplicitSender
    with WordSpecLike
    with Matchers
    with BeforeAndAfterAll {

  override def afterAll {
    TestKit.shutdownActorSystem(system)
  }

  "Token Generator Actor" must {
    "return a Token give a valid User message" in {

      val validateCredentialActor =
        system.actorOf(Props[TokenGeneratorActor])
      validateCredentialActor ! User("smontanez")

      expectMsgType[UserToken](5.seconds)

    }

    "return a Error give a invalid User message" in {

      val validateCredentialActor =
        system.actorOf(Props[TokenGeneratorActor])
      validateCredentialActor ! User("Asmontanez")

      expectMsgType[AuthError](5.seconds)

    }
  }
}
