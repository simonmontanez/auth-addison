package auth

import java.time.{Clock, Instant, ZoneId}
import java.time.temporal.ChronoUnit

class MockClock(@volatile private var clock: Clock) extends Clock {

  override def getZone: ZoneId = clock.getZone

  override def withZone(zone: ZoneId): Clock = clock.withZone(zone)

  override def instant(): Instant = Instant.now().truncatedTo(ChronoUnit.DAYS)
}
