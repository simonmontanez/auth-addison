package auth.http

import akka.http.scaladsl.server.Route
import akka.http.scaladsl.testkit.ScalatestRouteTest
import org.scalatest.{Matchers, WordSpec}
import akka.http.scaladsl.model.StatusCodes._

class HealthServiceRoutesTest
    extends WordSpec
    with Matchers
    with ScalatestRouteTest
    with HealthServiceRoutes {

  "HealthServiceRoutes" should {

    "return 200 status code" in {
      Get("/health-check") ~> Route.seal(health) ~> check {
        status shouldEqual OK
      }
    }
  }

}
