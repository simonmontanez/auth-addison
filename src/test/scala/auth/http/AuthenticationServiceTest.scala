package auth.http

import akka.actor.{ActorRef, Props}
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.model.StatusCodes._
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.testkit.{RouteTestTimeout, ScalatestRouteTest}
import akka.util.Timeout
import auth.domain.{Credentials, UserToken}
import auth.services.{TokenGeneratorActor, ValidateCredentialActor}
import org.scalatest.{Matchers, WordSpec}
import spray.json.DefaultJsonProtocol._
import spray.json.RootJsonFormat

import scala.concurrent.{ExecutionContext, Future}
import scala.concurrent.duration._

class AuthenticationServiceTest
    extends WordSpec
    with Matchers
    with ScalatestRouteTest
    with AuthenticationService {

  override val tokenGeneratorActor: ActorRef =
    system.actorOf(Props[TokenGeneratorActor])
  override val validateCredentialActor: ActorRef =
    system.actorOf(Props[ValidateCredentialActor])
  override implicit val askTimeout: Timeout =
    DefaultConfig.akkaActorTimeout.seconds
  override val executionContext: ExecutionContext = system.dispatcher
  override implicit val userM: RootJsonFormat[UserToken] = jsonFormat1(
    UserToken)
  implicit val timeout = RouteTestTimeout(10.seconds)

  override def requestToken(credentials: Credentials): Future[UserToken] = {
    if (credentials.username == "exception")
      Future.failed(new Exception("unexpected error"))
    else
      super.requestToken(credentials)
  }

  "AuthenticationService" should {

    "return token given username and password " in {
      Get("/token?username=smontanez&password=SMONTANEZ") ~> Route.seal(auth) ~> check {
        status shouldEqual OK
        responseAs[UserToken] shouldBe a[UserToken]
      }
    }

    "return Invalid credentials given bad credentials" in {
      Get("/token?username=smontanez&password=XXXX") ~> Route.seal(auth) ~> check {
        status shouldEqual Unauthorized
        responseAs[String] shouldBe "Invalid credentials"
      }
    }

    "return InternalServerError on unexpected error" in {
      Get("/token?username=exception&password=XXXX") ~> Route.seal(auth) ~> check {
        status shouldEqual InternalServerError
        responseAs[String] shouldBe "An error occurred validating the credentials"
      }
    }

  }
}
