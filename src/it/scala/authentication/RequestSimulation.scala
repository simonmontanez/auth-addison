package authentication

import io.gatling.core.Predef._
import io.gatling.core.structure.ScenarioBuilder
import io.gatling.http.Predef._
import io.gatling.http.protocol.HttpProtocolBuilder

import scala.concurrent.duration._

class RequestSimulation extends Simulation {

  val httpConf: HttpProtocolBuilder = http
    .baseURL("http://localhost:8080")

  val scn: ScenarioBuilder = scenario("Scenario 1")
    .exec(
      http("request_1")
        .get("/token?username=addison&password=ADDISON")
        .queryParam("username", "addison")
        .queryParam("password", "ADDISON"))

  setUp(
    scn.inject(constantUsersPerSec(10) during 10.seconds).protocols(httpConf))
}
