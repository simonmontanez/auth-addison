package auth.services

import akka.actor.Actor
import auth.domain.{Credentials, User}
import auth.errors.{AuthError, InvalidCredentials}

import scala.util.Random

class ValidateCredentialActor extends Actor with ValidateCredentialService {

  override def receive: Receive = {
    case c: Credentials =>
      Thread.sleep(Random.nextInt(5000).longValue()) // Some blocking task
      getUserByCredential(c).fold(
        error => sender() ! error,
        user => sender() ! user
      )
  }

}

trait ValidateCredentialService {
  def isValidCredential(c: Credentials): Boolean =
    c.username.toUpperCase.contentEquals(c.password)

  def getUserByCredential(c: Credentials): Either[AuthError, User] =
    if (isValidCredential(c))
      Right(User(c.username))
    else
      Left(InvalidCredentials)
}
