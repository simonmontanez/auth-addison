package auth.services

import java.time.Clock

import akka.actor.Actor
import auth.domain.{User, UserToken}
import auth.errors.{AuthError, InvalidUser}

import scala.util.Random

class TokenGeneratorActor() extends Actor with TokenGeneratorService {

  implicit val clock: Clock = Clock.systemUTC()

  override def receive: Receive = {
    case user: User =>
      Thread.sleep(Random.nextInt(5000).longValue()) // Some blocking task
      generateToken(user).fold(
        error => sender() ! error,
        token => sender() ! token
      )
  }

}

trait TokenGeneratorService {

  def isValidUser(u: User): Boolean = !u.userId.startsWith("A")

  def generateToken(u: User)(
      implicit clock: Clock): Either[AuthError, UserToken] = {
    if (isValidUser(u))
      Right(UserToken(s"${u.userId}_${clock.instant().toString}"))
    else Left(InvalidUser)
  }

}
