package auth.services
import akka.actor.ActorRef
import auth.domain.{Credentials, User, UserToken}

import scala.concurrent.{ExecutionContext, Future}
import akka.pattern.ask
import akka.util.Timeout
import auth.errors.AuthError

trait SimpleAsyncTokenService extends AsyncTokenService {

  val tokenGeneratorActor: ActorRef
  val validateCredentialActor: ActorRef

  implicit val askTimeout: Timeout
  implicit val executionContext: ExecutionContext

  override protected def authenticate(credentials: Credentials): Future[User] =
    (validateCredentialActor ? credentials).map {
      case user: User       => user
      case error: AuthError => throw error
    }

  override protected def issueToken(user: User): Future[UserToken] =
    (tokenGeneratorActor ? user).map {
      case token: UserToken => token
      case error: AuthError => throw error
    }

}
