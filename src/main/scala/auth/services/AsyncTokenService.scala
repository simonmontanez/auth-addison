package auth.services

import auth.domain.{Credentials, User, UserToken}
import scala.concurrent.ExecutionContext.Implicits.global //Todo use the other ExecutionContext
import scala.concurrent.Future

trait AsyncTokenService {
  protected def authenticate(credentials: Credentials): Future[User]
  protected def issueToken(user: User): Future[UserToken]

  def requestToken(credentials: Credentials): Future[UserToken] = {
    for {
      user <- authenticate(credentials)
      token <- issueToken(user)
    } yield token
  }
}
