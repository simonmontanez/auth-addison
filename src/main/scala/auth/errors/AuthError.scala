package auth.errors

sealed trait AuthError extends Exception {
  val msg: String
}

object InvalidCredentials extends AuthError {
  override val msg: String = "Invalid Credentials"
}
object InvalidUser extends AuthError {
  override val msg: String = "Invalid User"
}
