package auth.domain

case class UserToken(token: String)
