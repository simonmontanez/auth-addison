package auth.domain

case class Credentials(username: String, password: String)
