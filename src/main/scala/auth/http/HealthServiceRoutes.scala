package auth.http

import akka.http.scaladsl.model.StatusCodes._
import akka.http.scaladsl.server.Directives.{complete, _}
import akka.http.scaladsl.server.Route
import com.typesafe.scalalogging.LazyLogging
import scala.concurrent.duration._

trait HealthServiceRoutes extends LazyLogging {

  def health: Route =
    path("health-check") {
      withRequestTimeout(1.seconds) {
        get {
          complete(OK)
        }
      }
    }

}
