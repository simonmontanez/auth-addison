package auth.http

import com.typesafe.config.{Config, ConfigFactory}

object DefaultConfig {

  lazy val config: Config = ConfigFactory.load()

  lazy val httPort: Int = config.getInt("http.port")

  lazy val akkaActorTimeout: Int = config.getInt("akka.actor.timeout")

}
