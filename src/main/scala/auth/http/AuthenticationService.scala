package auth.http

import akka.http.scaladsl.server.{Directives, Route}
import auth.domain.{Credentials, UserToken}
import auth.services.SimpleAsyncTokenService
import spray.json.RootJsonFormat
import spray.json.DefaultJsonProtocol._
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.model.StatusCodes._
import auth.errors.AuthError
import com.typesafe.scalalogging.LazyLogging

import scala.util.{Failure, Success}

trait AuthenticationService
    extends Directives
    with SimpleAsyncTokenService
    with LazyLogging {

  implicit val userM: RootJsonFormat[UserToken] = jsonFormat1(UserToken)

  def auth: Route =
    path("token") {
      get {
        parameter("username".as[String], "password".as[String]) {
          (username, password) =>
            onComplete(requestToken(Credentials(username, password))) {
              case Success(user) => complete(user)
              case Failure(ex: AuthError) =>
                logger.error(
                  s"An error occurred validating the credentials ${ex.getMessage}")
                complete(Unauthorized -> s"Invalid credentials")
              case Failure(ex) =>
                logger.error(
                  s"An error occurred in the service ${ex.getMessage}")
                complete(
                  InternalServerError -> s"An error occurred validating the credentials")
            }
        }
      }
    }

}
