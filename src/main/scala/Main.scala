import java.util.concurrent.Executors

import akka.actor.{ActorSystem, Props}
import akka.http.scaladsl.Http
import akka.routing.{DefaultResizer, SmallestMailboxPool}
import akka.stream.ActorMaterializer
import akka.util.Timeout
import auth.http.{AuthenticationService, DefaultConfig, HealthServiceRoutes}
import auth.services.{TokenGeneratorActor, ValidateCredentialActor}
import com.typesafe.scalalogging.LazyLogging

import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext}
import scala.util.{Failure, Success}

object Main
    extends App
    with HealthServiceRoutes
    with AuthenticationService
    with LazyLogging {

  implicit val system: ActorSystem = ActorSystem("auth-system")
  implicit val materializer: ActorMaterializer = ActorMaterializer()

  val resizer = DefaultResizer(lowerBound = 8, upperBound = 16)

  val executionContext: ExecutionContext = ExecutionContext.fromExecutor(
    Executors.newWorkStealingPool()) //executionContext futures sending messages to the actors

  implicit val ec
    : ExecutionContext = system.dispatcher //executionContext http calls and pool actors

  val askTimeout: Timeout = DefaultConfig.akkaActorTimeout.seconds // :(
  val validateCredentialActor = system.actorOf(
    SmallestMailboxPool(8, Some(resizer)).props(Props[ValidateCredentialActor]))
  val tokenGeneratorActor = system.actorOf(
    SmallestMailboxPool(8, Some(resizer)).props(Props[TokenGeneratorActor]))

  //TODO: Change to bindAndHandleAsync
  val bindingFuture: Unit = Http().bindAndHandle(
    health ~ auth,
    "localhost",
    DefaultConfig.httPort) onComplete {
    case Success(Http.ServerBinding(localAddress)) =>
      logger.info(s"Http service started. Listening $localAddress ")
    case Failure(exception) =>
      logger.error("Error binding http service", exception)
      system.terminate()
  }

  sys.addShutdownHook {
    logger.info("Shutting down")
    Await.ready(system.terminate(), 10.seconds)
    ()
  }
}
