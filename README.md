# Authentication Service

![branch_coverage](https://img.shields.io/badge/branch_coverage-100%25-brightgreen.svg?longCache=true&style=flat-square) ![statement_coverage](https://img.shields.io/badge/statement_coverage-98.58%25-brightgreen.svg?longCache=true&style=flat-square)

## Overview

Simple service to get a token given a username and password

## Approach 

- Akka Http to expose the REST API
- Akka actors to handle the logic related to validate credentials and generate tokens


## Running and Testing

### Prerequisites

1. Just install SBT:
    * [SBT for Linux](http://www.scala-sbt.org/0.13/docs/Installing-sbt-on-Linux.html)
    * [SBT for Mac](http://www.scala-sbt.org/0.13/docs/Installing-sbt-on-Mac.html)
    * [SBT for Windows](http://www.scala-sbt.org/0.13/docs/Installing-sbt-on-Windows.html)
    
### Starting the service

The project exposes HTTP service using the port 8080

#### Running http locally

```bash
sbt
run
```
#### Getting token via HTTP endpoint

* `GET http://localhost:8080/token?username=0&password=0`


URL Query parameters.
`username`: Username.
`password`: Password.


Rules:

* If the password matches the username in uppercase, the service returns a JSON object with the format `{"token":"token_generated"}` otherwise it returns an error. 
* If the password matches the username in uppercase but the username starts with A, the service returns an error.




#### Running unit test

```bash
sbt
test
```

#### Get tests coverage report

```bash
sbt
review
```

#### Run [gatling](https://gatling.io) test

First run the http service

```bash
sbt
run
```

Then execute the gatling test. It will execute an scenario simulating 10 users per second during 10 seconds.

```bash
sbt
gatling-it:test
```

The source code is under the `it` folder
